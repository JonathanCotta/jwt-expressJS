const express = require("express");
const router  = express.Router();

const userCtlr = require("../controllers/userController");
const auth     = require("../middleware/auth");

router.get("/users", auth, userCtlr.getAll);
router.get("/user/:id", auth, userCtlr.getOne);
router.post("/user/", auth, userCtlr.create);
router.put("/user/", auth, userCtlr.update);
router.delete("/user/:id", auth, userCtlr.delete);

router.post("/authenticate", userCtlr.authenticate);
router.get("/seed", auth, userCtlr.seed);

module.exports = router;