const User   = require("../models/user");
const jwt    = require("jsonwebtoken");
const config = require("../config");

exports.getAll = (req, res) => {
    User.find({})
        .catch(err => res.json({ status: "fail", response: err }))
        .then(users => res.json(users));
}

exports.getOne = (req, res) => {
    User.findOne({ _id: req.params.id })
        .catch(err => res.json({ status: "fail", response: err }))
        .then(user => res.json(user));
}

exports.create = (req, res) => {

    User.create({
        name: req.body.name,
        password: User.genHash(req.body.password),
        admin: req.body.admin
    })
        .catch(err => res.json({ status: "fail", response: err }))
        .then(user => res.send(`User ${user.name} was successfully created`));

}

exports.update = (req, res) => {

    User.update({ _id: req.body.id }, { name: req.body.name, password: User.genHash(req.body.password), admin: req.body.admin })
        .catch(err => res.json({ status: "fail", response: err }))
        .then(raw => res.json({ status: "success", response: raw }));
}

exports.delete = (req, res) => {
    User.deleteOne({ _id: req.params.id })
        .catch(err => res.json({ status: "fail", response: err }))
        .then(() => res.json({ status: "success", response: "user successfully removed" }))
}

exports.authenticate = (req, res) => {
    User.findOne({ name: req.body.name }, (err, user) => {

        if (err) throw err;

        if (!user)
            res.json({ success: false, message: 'Authentication failed. User not found.' });

        if (!User.validHash(req.body.password, user.password))
            res.json({ success: false, message: `Authentication failed. Wrong password.` });

        const payload = { admin: user.admin };

        let token = jwt.sign(payload, config.secret, { expiresIn: "1 day" });

        res.json({ success: true, token: token });
    });
}

exports.seed = (req, res) => {
    let nick = new User({
        name: 'Nick Cerminara',
        password: 'password',
        admin: true
    });

    nick.save((err) => {
        if (err) throw err;

        console.log('User saved successfully');
        res.json({ success: true });
    });
}