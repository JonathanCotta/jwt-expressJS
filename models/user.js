const mongoose   = require('mongoose');
const bcrypt     = require("bcrypt");
const Schema     = mongoose.Schema;
const saltRounds = 10;

let userSchema = new Schema({
    name: String,
    password: String,
    admin: Boolean
});

userSchema.statics.genHash = function (pwd) {
    return bcrypt.hashSync(pwd, saltRounds);
}

userSchema.statics.validHash = function (data, hash) {
    return bcrypt.compareSync(data, hash);
}

module.exports = mongoose.model('User', userSchema);