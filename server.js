const express  = require("express");
const morgan   = require("morgan");
const mongoose = require("mongoose");

const config = require("./config");
const router = require("./routes");

const app  = express();
const PORT = process.env.PORT || 8888;

mongoose.connect(config.database, { useNewUrlParser: true });

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(morgan("dev"));

app.use("/api/", router);

app.listen(PORT, () => console.log(`server is runing on http://localhost:${PORT}/api/`));